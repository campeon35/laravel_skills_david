<?php 
	//PON AQUÍ LA DIRECCIÓN DE TU WSDL
	$wsdl = "http://127.0.0.1/laravel_skills_David/public/api/wsdl";

	$cliente = new SoapClient($wsdl);
	$centro = "IES Miguel Herrero";
	$tutor = "Lola Mento";
?>

<!DOCTYPE html>
<html>
<head>
	<title>Cliente SOAP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<h2 class="text-primary">Participantes del <?php echo $centro; ?></h2>
		<?php echo $cliente->getNumeroParticipantesCentro($centro); ?>

		<h2 class="mt-4 text-danger">Listado de participantes del tutor <?php echo $tutor; ?></h2>
		<ul>
			<?php 
				$participantes = $cliente->getParticipantesTutor($tutor);
				foreach ($participantes as $participante) 
				{
					echo "<li>{$participante->nombre} {$participante->apellidos}: {$participante->puntos} puntos</li>";
				}
			?>
		</ul>	
	</div>
</body>
</html>