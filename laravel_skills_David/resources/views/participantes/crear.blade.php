@extends('layouts.master')

@section('titulo')
	Participante
@endsection

@section('contenido')
	<div class="row">
		<div class="offset-md-3 col-md-6">
			<div class="card">
				<div class="card-header text-center">
					Añadir nuevo participante
				</div>
				<div class="card-body" style="padding:30px">
					<form action="{{ action('ParticipantesController@postCrear') }}" method="POST" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="form-group">
							<label for="nombre">Nombre</label>
							<input type="text" name="nombre" id="nombre" class="form-control">
						</div>
						<div class="form-group">
							<label for="apellidos">Apellidos</label>
							<input type="text" name="apellidos" id="apellidos" class="form-control">
						</div>
						<div class="form-group">
							<label for="centro">Centro</label>
							<input type="text" name="centro" id="centro" class="form-control">
						</div>
						<div class="form-group">
							<label for="tutor">Tutor</label>
							<input type="text" name="tutor" id="tutor" class="form-control">
						</div>
						<div class="form-group">
							<label for="fechaNacimiento">Fecha de nacimiento</label>
							<input type="text" name="fechaNacimiento" id="fechaNacimiento" class="form-control" placeholder="dd/mm/yyyy">
						</div>
						
						<div class="form-group">
							<label for="historial">Modalidad</label>
							<select name="modalidad" id="modalidad" class="form-control">
								@foreach($modalidades as $modalidad)
									<option value="{{ $modalidad->id }}">{{ $modalidad->nombre }}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label for="foto">Foto</label>
							<br>
							<input type="file" name="foto" id="foto">
						</div>
						<div class="form-group text-center">
							<button type="submit" class="btn btn-success" style="padding:8px 100px; margin-top:25px;">
								Añadir participante
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<script>
        $(document).ready(function() {
            $('#tutor').autocomplete({
                source: 
                function (query, result) 
                {
                    $.ajax({
                        type: "POST",
                        url: "{{url('busquedaAjax')}}",
                        dataType: "json",
                        data: {"_token": "{{ csrf_token() }}","tutor": query['term']},
                        success : function(data){
                           result(data);
                        }
                    });
                },
                position: {
                  	my: "left+0 top+8", //Para mover la caja 8px abajo
                }
            });
      });
</script>
@endsection