@extends('layouts.master')

@section('titulo')
	Modalidades skills 2020
@endsection

@section('contenido')
	<h1>{{ $modalidad->nombre }}</h1>
	<h4>Familia profesional: <span style="font-size: 15px;">{{ $modalidad->familiaProfesional }}</span></h4>
	<h4>Participantes</h4>
	<div class="row">
		@foreach($modalidad->participantes as $indice => $participante)
			<div style="position: static; display: inline-block;">
				<p>{{ $participante->nombre }}</p>
				<img src="../../assets/imagenes/participantes/{{ $participante->imagen }}">
			</div>
		@endforeach
	</div>
	<br>
	<a class="btn btn-warning" href="../puntuar/{{ $modalidad->slug }}" role="button">Puntuar</a>
	<a class="btn btn-outline-dark" href="../resetear/{{ $modalidad->slug }}" role="button">Resetear</a>
	<br>

	@if(count($modalidad->participantes) > 0)
		@if($modalidad->clasificacion[0]->puntos > -1)
			<table>
				<tr>
					<th>Nombre</th>
					<th>Puntos</th>
				</tr>
				@foreach($modalidad->clasificacion as $participante)
					<tr>
						<td>{{ $participante->nombre }}</td>
						<td>{{ $participante->puntos }}</td>
					</tr>
				@endforeach
			</table>
		@endif
	@endif
@endsection