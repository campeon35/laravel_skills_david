@extends('layouts.master')

@section('titulo')
	Modalidades skills 2020
@endsection

@section('contenido')
	
	<div class="row">
		@foreach($modalidades as $clave => $modalidad)
			<div class="col-xs-12 col-sm-6 col-md-4" style="border: solid red 1px; padding: 10px">
				<a href="{{ url('/modalidades/mostrar/' . $modalidad->slug ) }}">
					<img src="assets/imagenes/modalidades/{{ $modalidad->imagen }}" style="height:120px"/>
					<label style="float: right; font-size:20px; margin:5px 0 10px 0">
						{{ $modalidad->nombre }}
						<br>
						<label style="font-size: 15px">{{ count($modalidad->participantes) }} participantes</label>
					</label>
				</a>
			</div>
		@endforeach
	</div>
@endsection