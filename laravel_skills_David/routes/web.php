<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('prueba', function(){
	return App\Participante::where('tutor', 'Lola Mento')->orderBy('puntos', 'desc')->get();
});


Route::get('/', function(){
	return redirect('modalidades');
});

Route::get('modalidades', 'ModalidadesController@getModalidades');

Route::get('modalidades/mostrar/{slug}', 'ModalidadesController@getVer');

Route::get('modalidades/puntuar/{slug}', 'ModalidadesController@getPuntuar');

Route::get('modalidades/resetear/{slug}', 'ModalidadesController@getResetear');

Route::get('participantes/inscribir', 'ParticipantesController@getCrear');
Route::post('participantes/inscribir', 'ParticipantesController@postCrear');

//SoapServer
Route::any('api', 'SoapServerController@getServer');
Route::any('api/wsdl', 'SoapServerController@getWSDL');

//AJAX
Route::post('busquedaAjax', 'ParticipantesController@tutor');