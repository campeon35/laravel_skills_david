<?php

use Illuminate\Database\Seeder;
use App\Modalidad;
use App\Participante;

class DatabaseSeeder extends Seeder
{
	private $arrayModalidad = [
		[	"nombre" => "Desarrollo Web",
			"familiaProfesional" => "Informática y Comunicaciones",
			"imagen" => "desarrollo-web.png"
		],
		[	"nombre" => "Diseño y Animaciones de Juegos 3D",
			"familiaProfesional" => "Informática y Comunicaciones",
			"imagen" => "animacion-3d.png"
		],
		[	"nombre" => "Floristería",
			"familiaProfesional" => "Artes Creativas",
			"imagen" => "floristeria.png"
		],
		[	"nombre" => "Soldadura",
			"familiaProfesional" => "Fabricación",
			"imagen" => "soldadura.png"
		],
		[	"nombre" => "Mecatrónica",
			"familiaProfesional" => "Fabricación",
			"imagen" => "mecatronica.png"
		],
		[	"nombre" => "Servicio de Restaurante y Bar",
			"familiaProfesional" => "Servicios",
			"imagen" => "servicio-restaurante.png"
		],
		[	"nombre" => "Carpintería",
			"familiaProfesional" => "Construcciones e Instalaciones",
			"imagen" => "carpinteria.png"
		],
		[	"nombre" => "Estética",
			"familiaProfesional" => "Servicios",
			"imagen" => "estetica.png"
		],
		[	"nombre" => "Pintura del Automóvil",
			"familiaProfesional" => "Transporte y Logística",
			"imagen" => "pintura-automovil.png"
		],


	];
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('participantes')->delete();
    	DB::table('modalidades')->delete();
        $this->seedModalidades();
        $this->seedParticipante();

    }


    private function seedModalidades()
    {
    	foreach ($this->arrayModalidad as $modalidad) 
    	{
    		$m = new Modalidad();
    		$m->nombre = $modalidad['nombre'];
    		$m->slug = Str::slug($modalidad['nombre']);
    		$m->familiaProfesional = $modalidad['familiaProfesional'];
    		$m->imagen = $modalidad['imagen'];
    		$m->save();
    	}
    }

    private function seedParticipante()
    {
    	$p = new Participante();
    	$p->nombre = "Andrés";
    	$p->apellidos = "Trozado";
    	$p->centro = "IES Augusto González de Linares";
    	$p->tutor = "Lola Mento";
    	$p->fechaNacimiento = "2000-01-01";
    	$p->imagen = "participante-11.png";
    	$p->modalidad_id = Modalidad::where('nombre','like',"%web%")->first()->id;
    	$p->save();


    	$p = new Participante();
    	$p->nombre = "Enrique";
    	$p->apellidos = "Cido";
    	$p->centro = "IES Miguel Herrero";
    	$p->tutor = "Lola Mento";
    	$p->fechaNacimiento = "2000-07-07";
    	$p->imagen = "participante-12.png";
    	$p->modalidad_id = Modalidad::where('nombre','like',"%web%")->first()->id;
    	$p->save();


    	$p = new Participante();
    	$p->nombre = "Alberto";
    	$p->apellidos = "Mate";
    	$p->centro = "IES Miguel Herrero";
    	$p->tutor = "Lola Mento";
    	$p->fechaNacimiento = "2000-09-11";
    	$p->imagen = "participante-07.png";
    	$p->modalidad_id = Modalidad::where('nombre','like',"%web%")->first()->id;
    	$p->save();


    	$p = new Participante();
    	$p->nombre = "Estela";
    	$p->apellidos = "Gartija";
    	$p->centro = "IES Miguel Herrero";
    	$p->tutor = "Lola Mento";
    	$p->fechaNacimiento = "2001-05-14";
    	$p->imagen = "participante-05.png";
    	$p->modalidad_id = Modalidad::where('nombre','like',"%web%")->first()->id;
    	$p->save();


    	$p = new Participante();
    	$p->nombre = "Elsa";
    	$p->apellidos = "Capunta";
    	$p->centro = "IES Besaya";
    	$p->tutor = "Aitor Tilla";
    	$p->fechaNacimiento = "2001-02-04";
    	$p->imagen = "participante-01.png";
    	$p->modalidad_id = Modalidad::where('nombre','like',"%soldadura%")->first()->id;
    	$p->save();


    	$p = new Participante();
    	$p->nombre = "Elena";
    	$p->apellidos = "Nito";
    	$p->centro = "IES Zapatón";
    	$p->tutor = "Helen Chufe";
    	$p->fechaNacimiento = "2000-11-15";
    	$p->imagen = "participante-02.png";
    	$p->modalidad_id = Modalidad::where('nombre','like',"%restaurante%")->first()->id;
    	$p->save();

    	$p = new Participante();
    	$p->nombre = "Susana";
    	$p->apellidos = "Torio";
    	$p->centro = "IES Miguel Herrero";
    	$p->tutor = "Lola Mento";
    	$p->fechaNacimiento = "2001-05-30";
    	$p->imagen = "participante-03.png";
    	$p->modalidad_id = Modalidad::where('nombre','like',"%web%")->first()->id;
    	$p->save();


    	$p = new Participante();
    	$p->nombre = "Paca";
    	$p->apellidos = "Garte";
    	$p->centro = "IES Besaya";
    	$p->tutor = "Rubén Fermizo";
    	$p->fechaNacimiento = "2002-03-19";
    	$p->imagen = "participante-04.png";
    	$p->modalidad_id = Modalidad::where('nombre','like',"%floristería%")->first()->id;
    	$p->save();
    }

    
}
