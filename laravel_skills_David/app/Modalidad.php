<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $nombre
 * @property string $slug
 * @property string $familiaProfesional
 * @property string $imagen
 */
class Modalidad extends Model
{
    protected $table = 'modalidades';

    /**
	 * Participantes de la modalidad
	 * @return App\Participante[]
	 */
    public function participantes(){
		return $this->hasMany('App\Participante');
	}

	/**
	 * Participantes de la modalidad ordenados por puntos (mayor a menor)
	 * @return App\Participante[]
	 */
	public function clasificacion(){
		return $this->hasMany('App\Participante')->orderBy('puntos','desc');
	}
}
