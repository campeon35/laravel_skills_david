<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $nombre
 * @property string $apellidos
 * @property string $centro
 * @property string $tutor
 * @property string $fechaNacimiento
 * @property string $imagen
 */
class Participante extends Model
{
	protected $table = 'participantes';

	/**
	 * Modalidad a la que pertenece el participante
	 * @return App\Modalidad
	 */
	public function modalidad(){
		return $this->belongsTo('App\Modalidad');
	}
}
