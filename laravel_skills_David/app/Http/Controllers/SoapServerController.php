<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Modalidad;
use App\Participante;
use SoapServer;
use App\Lib\WSDLDocument;

class SoapServerController extends Controller
{
	private $clase = '\\App\\Http\\Controllers\\SkillsWebService';
	private $uri = 'http://127.0.0.1/laravel_skills_David';
	private $servidor = 'http://127.0.0.1/laravel_skills_David/public/api';
	private $urlWSDL = 'http://127.0.0.1/laravel_skills_David/public/api/wsdl';

    function getServer(){
		$server = new SoapServer($this->urlWSDL);
    	$server->setClass($this->clase);
    	$server->handle();
    	exit();
    }

    function getWSDL(){
	    $wsdl = new WSDLDocument($this->clase, $this->servidor, $this->uri);
		$wsdl->formatOutput = true;
		header('Content-Type: text/xml');
		echo $wsdl->saveXML();
    }
}

class SkillsWebService
{
	/**
	 * Participantes de un centro
	 * @param string $centro
	 * @return integer
	 */
	function getNumeroParticipantesCentro($centro){
		$participantes = Participante::all()->where('centro', $centro);
		return count($participantes);
	}

	/**
	 * Array de participantes con el tutor del parámetro
	 * @param string $tutor
	 * @return App\Participante[]
	 */
	function getParticipantesTutor($tutor){
		return App\Participante::where('tutor', $tutor)->orderBy('puntos', 'desc')->get();
	}
}