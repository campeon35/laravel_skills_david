<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modalidad;

class ModalidadesController extends Controller
{
    public function getModalidades() {
		$modalidades = Modalidad::all();
		return view('modalidades.index', array('modalidades' => $modalidades));
	}

    public function getVer($slug) {
    	$modalidad = Modalidad::where('slug', $slug)->first();
		return view('modalidades.mostrar', array('modalidad' => $modalidad));
	}

	public function getPuntuar($slug){
		$modalidad = Modalidad::where('slug', $slug)->first();
		$participantes = $modalidad->participantes;
		foreach ($participantes as $participante) {
			$participante->puntos = rand(0,100);
			$participante->save();
		}
		return redirect('modalidades/mostrar/'.$slug);
	}

	public function getResetear($slug){
		$modalidad = Modalidad::where('slug', $slug)->first();
		$participantes = $modalidad->participantes;
		foreach ($participantes as $participante) {
			$participante->puntos = -1;
			$participante->save();
		}
		return redirect('modalidades/mostrar/'.$slug);
	}
}
