<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modalidad;
use App\Participante;

class ParticipantesController extends Controller
{
    public function getCrear() {
    	$modalidades = Modalidad::all();
		return view('participantes.crear', array('modalidades' => $modalidades),);
	}
	
	public function postCrear(Request $request) {
		$p = new Participante();

		$p->nombre = $request->nombre;
		$p->apellidos = $request->apellidos;
		$p->centro = $request->centro;
		$p->tutor = $request->tutor;
		$p->fechaNacimiento = $request->fechaNacimiento;
		$p->modalidad_id = Modalidad::findOrFail($request->modalidad)->id;
		
		if (!empty($request->foto) && $request->foto->isValid()) {
			$p->imagen = $request->foto->store('', 'participantes');
		}
		
		try{
			$p->save();
			return redirect('/');

		} catch(\Illuminate\Database\QueryException $ex){
			
			echo "<pre>";
			var_dump($p);
			
			//return redirect('/');
		}
	}

	public function tutor(Request $request){
		$tutores = Participante::select('tutor')->where('tutor', 'like', "%$request->tutor%")->pluck('tutor');
		return response()->json_encode($tutores);
	}
}
